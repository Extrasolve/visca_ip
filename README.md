# README #

## Visca over IP (v1.2 Updated 2024)

![screenshot](./graphics/screenshot.jpg)  

This plugin has been updated following user request.  
Basic operation is the same as initial release but with the following improvements:

    * New protocol property allows selection of either 'Sony VISCA' or 'Raw VISCA' formats.
      The difference is that the Sony VISCA over IP protocol specifies additional length and
      sequence bytes to be added to the packet, the Raw option omits these.
      
    * Up to 10 Custom commands.  Just enter the visca in the format \x81 .... \xff
      Example uses include:
        Enabling and disabling auto tracking
        Tally LED on and Off

This plugin should work for all cameras that support visca over IP.
The port number is changeable as some cameras do not use the default 52381.

The module supports 0-128 presets.  It is also possible to enable 'push and hold' of the
preset recall buttons to allow saving after a specified hold time

![properties](./graphics/properties.jpg)  

It has been tested on a BirdDog P100 camera

Note:
    This module used UDP Only.  The connected LED only works if the camera replies
    to model information request. Many cameras do not respond.
    In these situations, simply use a 'Ping' component to confirm device is present.

    To install the plugin simply download 'VISCA_IP.qplug' and double click file
    QSys should prompt to install / replace the plugin.

If you have any questions email or call me me directly
info@extrasolve.co.uk or call +44 7792005619